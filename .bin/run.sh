#!/bin/bash

DIR="$( cd "$(dirname "$0")" ; pwd -P | sed -e 's/.bin//g')"

# Start app - staging
function run_staging() {
    SETTINGS="--settings core.settings.staging"
    cd ${DIR}/web && \
    python3.6 manage.py makemigrations $SETTINGS && \
    python3.6 manage.py migrate $SETTINGS && \
    python3.6 manage.py runserver 0.0.0.0:80 $SETTINGS
}

# Start app - production
function run_production() {
    cd ${DIR}/web && \
    gunicorn --bind unix:/tmp/bars161ru.sock core.wsgi --log-level debug --access-logfile /var/log/gunicorn.access --error-logfile=/var/log/gunicorn.error
}

# Start app
if [ $1 = "staging" ]; then

# Satging env
  run_staging
elif [ $1 = "production" ]; then

# Production env
  run_production
fi  
