from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
STATIC_URL = '/'
ALLOWED_HOSTS = ['62.109.27.67', 'bars161.ru']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('POSTGRES_DB'),
        'USER': env('POSTGRES_USER'),
        'PASSWORD': env('POSTGRES_PASSWORD'),
        'HOST': env('POSTGRES_HOST'),
        'PORT': env('POSTGRES_PORT'),
    }
}