from django import template

register = template.Library()


@register.filter(name='get_class')
def get_class(value, arg):
    """Возвращает текущий css класс меню"""
    if ''.join(('/', value.replace('/', ''))) == arg:
        return 'active'
    return ''
