import json
from django.views import View
from django.http import HttpResponse
from django.template import loader
from page.models import Page as PageModel
from instagram.models import InstagramImages


class Page(View):
    @staticmethod
    def get(request, **kwargs):
        page = PageModel.page_public.filter(slug=kwargs.get('slug')).get()
        instagram = InstagramImages.objects.all().order_by('-created_time_instagram')
        context = {'page': page, 'content': page.content_set.get()}
        if instagram:
            context.update({'instagram': instagram})
        template = loader.get_template(
            ''.join((page.template, '.html')) if page.template else 'index.html')
        return HttpResponse(template.render(context, request))
